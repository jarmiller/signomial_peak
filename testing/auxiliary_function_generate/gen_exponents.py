import numpy as np

n=1
A1_set = np.array([[1, 0], [0, 0.5], [0, -0.5], [0, -0.3]])
d_set = 4

def gen_alpha(A1, d):
    # A0 = np.array(([[0, 0]]))
    A0 = np.zeros([1, A1.shape[1]])

    A = np.concatenate([A0, A1], 0)

    for i in range(1, d):
        Akron = np.kron(np.ones((A1.shape[0], 1)), A) + np.kron(A1, np.ones((A.shape[0], 1)))
        A = np.unique(Akron, axis=0)

    return A


A_out = gen_alpha(A1_set, d_set)

print("done!")

