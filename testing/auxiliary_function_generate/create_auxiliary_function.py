#testing the feasibility of adding an auxiliary function variable to the SAGEOPT framework
#code below is based on scripts/anish/play_sigdomains.py

import numpy as np
import sageopt.coniclifts as cl
import sageopt as so
from scripts.anish.column_generation_no_hierarchy import create_support_function_constraints
from scripts.anish.column_generation_no_hierarchy import separation_problem
n = 1 #number of variables
sys_vars = cl.Variable(shape=(n+1,), name='temp')
t_var = sys_vars[0]
x_var = sys_vars[1:]

x0_var = cl.Variable(shape=(n,), name='temp_0')

ind_const = 0;

# t_var = cl.Variable(shape=(1,), name='t')
# x_var = cl.Variable(shape=(n,), name='x')



#exponent vectors for signomials (such as being found in dynamics)
#indexed as [t, x]
A0 = np.array(([[0, 0]]))
A1 = np.array([[1, 0], [0, 1], [0, 0.5], [1, -0.5], [0, -0.3]])
A2 = np.unique(np.kron(np.ones((2, 1)), A1) + np.kron(A1, np.ones((2, 1))), axis=0) #there is a more efficient way to do this probably

A = np.unique(np.concatenate([A0, A1, A2], 0), axis=0)

nsig = A.shape[0]
#there is also a more efficient way to do the constant offset (probably)
ind_const = [i for i in range(A.shape[0]) if np.all(A[i, :]==np.zeros(n+1))];




# Construct the support set of the variables ([-1, 1]^n constraints)
#create support constraints
cons_t = [t_var <=1, t_var>=0]
cons_x = []
for i in range(n):
    cons_x.append(x_var[i] <= 1)
    cons_x.append(x_var[i] >= -1)

cons_x0 = []
for i in range(n):
    cons_x0.append(x0_var[i] <= 0.2)
    cons_x0.append(x0_var[i] >= 0)

cons_all = cons_t + cons_x;
#store the support constraints
#initial set:
X0 = so.SigDomain(n)
X0.parse_coniclifts_constraints(cons_x0)

#time-space set
Xall = so.SigDomain(n+1)
Xall.parse_coniclifts_constraints(cons_all)

# create an auxiliary function
d = 1

gamma = cl.Variable(shape=(), name='gamma')
vc = cl.Variable(shape=(nsig, 1), name='v')


#because we are dealing with signomials, we have v(t, x) = sum(e^(at * t + ax'*x)).
#so when t=0, we have v(t, x) = sum(e^(ax'*x)). The t terms of the A matrix get dropped.
#check that this works in chemical reaction network structures.

vA = A
vA0 = np.reshape(A[:, 1:], (nsig, 1))


#constraints for peak estimation:

#initial constraint
# gamma - v(0, x) >= 0 over X0
# v0_expr = cl.Expression()
vneg = -vc;
nonneg_init = cl.Expression(vneg.copy());
nonneg_init[ind_const] += gamma

constr_init = cl.PrimalSageCone(nonneg_init, vA0, X=X0, name='initial')



# v(t, x) >= p(x) over [0, T] times X

#find maximum value of x(1)
pA = np.array([[0, 1]])
pc = 1
peak_A = np.concatenate((vA, pA), axis=0)

vaug = vc + [pc]
nonneg_peak = cl.Expression(vaug.copy())
constr_peak = cl.PrimalSageCone(nonneg_init, peak_A, X=Xall, name='peak')

# Lie_f v(t, x) <= 0 over [0, T] times X (do this one last)
# need to perform an algorithmic rule for lie differentiation



k = 2+2;
