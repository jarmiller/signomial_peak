import sageopt.coniclifts as cl
import numpy as np
# random problem data
n = 6
m = 3;
G = np.random.randn(m, n)
h = np.random.rand(m)
c = np.random.rand(n)
# input to coniclift's Problem constructor
x = cl.Variable(shape=(n,))
constrs = [0 <= x, G @ x == h]
objective_expression = c @ x
prob = cl.Problem(cl.MIN, objective_expression, constrs)
prob.solve(solver='ECOS', verbose=False)
x_opt = x.value


print("done!")