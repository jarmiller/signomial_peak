# https://matplotlib.org/stable/gallery/images_contours_and_fields/plot_streamplot.html
import matplotlib.pyplot as plt
import numpy as np

w = 3
Y, X = np.mgrid[-w:w:100j, -w:w:100j]
A = np.array([[0.7, 0.5], [0.8, 0.4]])
exp_s = 0.9
exp_i = 0.8

# U = -1 - X**2 + Y
# V = 1 + X - Y**2
U = np.exp(-X) * ( A[0,0]*np.exp(X) - A[0, 1]*np.exp(exp_s*X+exp_i*Y))
V = np.exp(-Y) * (-A[1,0]*np.exp(Y) + A[1, 1]*np.exp(exp_s*X+exp_i*Y))
speed = np.sqrt(U**2 + V**2)

# fig, axs = plt.subplots(3, 2, figsize=(7, 9), height_ratios=[1, 1, 2])
# axs = axs.flat

x0 = [0.5, 1]
y0 = np.log(x0)
#  Varying density along a streamline
plt.streamplot(X, Y, U, V)
plt.scatter(y0[0], y0[1])
plt.show()

