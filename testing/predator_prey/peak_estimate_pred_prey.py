#attempt peak estimation for a fractional SIR system
import numpy as np
import sageopt
import sageopt.coniclifts as cl
from scipy.integrate import odeint

def model_log(y, t):
    A = np.array([[0.7, 0.8], [0.8, 0.4]])
    exp_s = 0.9
    exp_i = 0.8

    predation = np.exp(exp_s*y[0] + exp_i*y[1])

    dydt = [A[0,0]*np.exp(y[0]) - A[0,1]*predation, A[1,1]*predation - A[1,0]*np.exp(y[1])]

    dydt_log = np.multiply(np.exp(-y), dydt)
    # dydt_log = dydt
    # dydt = -k * y
    return dydt_log

#helper functions
def gen_alpha(A1, d):
    # A0 = np.array(([[0, 0]]))
    A0 = np.zeros([1, A1.shape[1]])

    A_base = np.concatenate([A0, A1], 0)

    A = A_base
    for i in range(1, d):
        Akron = np.kron(np.ones((A_base.shape[0], 1)), A) + np.kron(A_base, np.ones((A.shape[0], 1)))
        A = np.unique(Akron, axis=0)

    return A

def eval_time(sig, t):
    alpha_x = sig.alpha[:, 1:]
    c_x = cl.Expression(sig.c.copy())
    for j in range(sig._m):
        c_x[j] *= np.exp(t*sig.alpha[j, 0])

    p = sageopt.Signomial(alpha_x, c_x)
    return p

def sig_grad(sig, i):
    """ (trying to debug Signomial._partial from Signomials.py)
    Compute the symbolic partial derivative of this signomial, at coordinate ``i``.

    Parameters
    ----------
    i : int
        ``i`` must be an integer from 0 to ``self.n - 1``.

    Returns
    -------
    p : Signomial
        The function obtained by differentiating this signomial with respect to its i-th argument.
    """
    c_partial = cl.Expression(sig.c.copy())
    for j in range(sig._m):
        vec = sig.alpha[j, :]
        c_partial[j] = sig.c[j] * vec[i]

    p = sageopt.Signomial(sig.alpha, c_partial)
    return p

#define the objective
n = 2
vars = sageopt.standard_sig_monomials(n+1)
expt = vars[0]
y = vars[1:]

#parameters of dynamics
# beta = 1
# gamma = 1


A = np.array([[0.7, 0.8], [0.8, 0.4]])
exp_s = 0.9
exp_i = 0.8

# exp_s = 0.85
# exp_i = 0.65


#define the auxiliary function
# A1_set = np.array([[0, 0, 0], [1, 0, 0], [0, 0.5, 0], [0, -0.5, 0], [0, 0.8, 0], [0, -0.8, 0],  [0, 0, 0.5], [0, 0, -0.5]])
# A1_set = np.array([[0.5, 0, 0], [1, 0, 0], [0, 0.8, 0], [0, -0.8, 0], [0, 0.3, 0], [0, -0.3, 0],  [0, 0, 0.8], [0, 0, -0.8], [0, 0, -0.3], [0, 0, -0.3]])
# A1_set = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1], [0, 0.5, 0.5]])
# A1_set = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1], [0, -1, 0], [0, 0, -1], [0, 0.5, 0.5]])
# A1_set = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1], [0, exp_s, exp_i], [0, -exp_s, -exp_i]])

# A1_set = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1], [0, -1, 0], [0, 0, -1], [0, exp_s, exp_i]])

# A1_set = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1], [0, exp_s-1, exp_i], [0, exp_s, exp_i-1]])
# d=3

# A1_set = np.array([[1, 0, 0], [0, 0, -1], [0, -1, 0], [0, 0, 1], [0, 1, 0]])
# d=6


#standard monomials
A1_set = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
d = 5

#initial point or initial circle?
INIT_POINT = True

v_alpha = gen_alpha(A1_set, d)
v_c = cl.Variable(shape=(v_alpha.shape[0],))

v = sageopt.Signomial(v_alpha, v_c)

#define the dynamics

# Tmax =16;
Tmax = 20;
# Tmax = 18;
# Tmax = 8;
#A: [[1, 0, 0], [0, 1, 0], [0, 0, 1], [0, exp_s, 0], [0, 0, exp_i]]
#d=4: 3
#d=5: 2.6331376407
#d=

# Tmax = 5;
#define the objective
p = y[1]

#dynamics are [f1, f2] in log-space
# exp_s = 0.8\
# exp_i = 0.6
f1_c = np.array([A[0,0], -A[0, 1]])
f1_alpha = np.array([[0, 1, 0], [0, exp_s, exp_i]])
f1_alpha_log = f1_alpha - np.array([[0, 1, 0]])
f1 = sageopt.Signomial(f1_alpha_log, f1_c)

# f2_c = np.array([[beta], [-gamma]])
f2_c = np.array([A[1,1], -A[1,0]])
f2_alpha = np.array([[0, exp_s, exp_i], [0, 0, 1]])
f2_alpha_log = f2_alpha - np.array([[0, 0, 1]])
f2 = sageopt.Signomial(f2_alpha_log, f2_c)

#define the support sets
sys_vars = cl.Variable(shape=(n+1,), name='temp')
t_var = sys_vars[0]
x_var = sys_vars[1:]

cons_t = [t_var <=1, t_var>=0]
cons_x = []
#box constraints
cons_x.append(x_var[0] >= -1.5)
cons_x.append(x_var[0] <= 2.4)
cons_x.append(x_var[1] >= -1.5)
cons_x.append(x_var[1] <= 1.5)

# xrange = [1e-3, 3]
# for i in range(n):
#     # cons_x.append(x_var[i] >= np.log(xrange[0]))
#     # cons_x.append(x_var[i] <= np.log(xrange[1]))
#
#     cons_x.append(x_var[i] >= -2.5)
#     cons_x.append(x_var[i] <= 2.5)

cons_all = cons_t + cons_x;
Xall = sageopt.SigDomain(n+1)
Xall.parse_coniclifts_constraints(cons_all)


#Initial condition
Tstart = 0

x0 = np.array([[0.5], [1]])
# x0 = np.array([[0.8], [1]])
y0 = np.log(x0)

# x0 = np.array([[np.exp(Tstart)], [0.5], [1]])
# x0x = x0[1:]
# y0 = np.log(x0)
# y0x = y0[1:]
if INIT_POINT:
    X0 = []
else:

    # x0_var = cl.Variable(shape=(n,), name='temp_0')
    # cons_x0 = [cl.vector2norm(x0_var-np.transpose(y0x)) <= 0.1]

    R2 = 0.4;
    cons_x0 = [R2 - sum((y-x0x)**2)>= 0]
    # cons_x0 = [cl.vector2norm(y-x0x) <= R2]

    X0 = sageopt.SigDomain(n)
    X0.parse_coniclifts_constraints(cons_x0)

# for i in range(n):
#     cons_x0.append(x0_var[i] <= 0.2)
#     cons_x0.append(x0_var[i] >= 0)

cons_all = cons_t + cons_x

#Lie nonnegativity
dvdt = sig_grad(v, 0)
dvdx = (sig_grad(v, 1)*f1) + (sig_grad(v, 2)*f2)

Lv = dvdt + Tmax*dvdx
nonneg_lie = -Lv
constr_lie = cl.PrimalSageCone(nonneg_lie.c, nonneg_lie.alpha, X=Xall, name='lie')

#peak value
nonneg_peak = v - p
constr_peak = cl.PrimalSageCone(nonneg_peak.c, nonneg_peak.alpha, X=Xall, name='peak')


#initial condition
v0 = eval_time(v, Tstart)
if INIT_POINT:
    # [0.5, 1]
    v00 = v0(y0)

    objective = v00
    constr_init = (objective >= 0)
    nonneg_init = objective
else:
    gamma = cl.Variable(shape=(1,))
    # nonneg_init = cl.Expression(gamma) - v0
    nonneg_init = -v0
    const_ind = nonneg_init.constant_location();
    nonneg_init.c[const_ind] += gamma
    constr_init = cl.PrimalSageCone(nonneg_init.c, nonneg_init.alpha, X=X0, name='init')
    objective = gamma

#put the problem together
# slack = cl.Variable(shape=(1,))
# constr_obj = slack <= objective
constr_obj = [objective >= 0]
constr = [constr_lie, constr_peak, constr_init]+ constr_obj


#solve the problem
prob = cl.Problem(cl.MIN, objective, constr)

status, val = prob.solve(solver='CP-MOSEK', verbose=True)

#recover the solution
vc_rec = v.c.value;
v_rec = sageopt.Signomial(v_alpha, vc_rec)

if not INIT_POINT:
    nonneg_init_rec = sageopt.Signomial(nonneg_init.alpha, nonneg_init.c.value)
else:
    nonneg_init_rec = v00.value

nonneg_peak_rec = sageopt.Signomial(nonneg_peak.alpha, nonneg_peak.c.value)
nonneg_lie_rec = sageopt.Signomial(nonneg_lie.alpha, nonneg_lie.c.value)

#test on a reference trajectory
Nt = 400
tref = np.linspace(0, Tmax, Nt)
y_center = odeint(model_log, y0[:,0], tref)
i_max_center = max(y_center[:, 1])


for i in range(Nt):
    pass
# solutions = sageopt.sig_solrec(prob)
print(f'peak bound {val}')
print("done!")


