import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

# from the tutorial https://apmonitor.com/pdc/index.php/Main/SolveDifferentialEquations
# function that returns dy/dt
def model(y,t):
    # k = 0.3

    A = np.array([[0.7, 0.8], [0.8, 0.4]])
    exp_s = 0.9
    exp_i = 0.8

    dydt = [A[0,0]*y[0] - A[0,1]*(y[0]**exp_s)*(y[1]**exp_i), A[1,1]*(y[0]**exp_s)*(y[1]**exp_i) - A[1,0]*y[1]]

    # exp_s = 1
    # exp_i = 1

    # dydt = [-beta*(y[0]**exp_s)*(y[1]**exp_i), beta*(y[0]**exp_s)*(y[1]**exp_i) - gamma*y[1]]
    # dydt = -k * y
    return dydt

def model_log(y, t):
    A = np.array([[0.7, 0.8], [0.8, 0.4]])
    exp_s = 0.9
    exp_i = 0.8

    predation = np.exp(exp_s*y[0] + exp_i*y[1])

    dydt = [A[0,0]*np.exp(y[0]) - A[0,1]*predation, A[1,1]*predation - A[1,0]*np.exp(y[1])]

    dydt_log = np.multiply(np.exp(-y), dydt)
    # dydt_log = dydt
    # dydt = -k * y
    return dydt_log

# initial condition
LOG = True
# y0 = [0.1, 1]

# y0 = [0.5, 1]
y0 = [0.8, 1]


y0_log = np.log(y0)


# time points
Tmax = 20
t = np.linspace(0,Tmax, 400)

#sample equations
y = odeint(model,y0,t)
y_log = odeint(model_log,y0_log,t)

# solve ODE
# if LOG:
#     title = "Epidemic (Standard)"
# else:
#     title = "Epidemic (Log)"

# plot results

i_max = max(y[:, 1])

print(f'peak predator {i_max}')
fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
ax1.plot(t,y[:, 0])
ax1.plot(t,y[:, 1])
ax1.set_xlabel('time')
ax1.set_title("Predator-Prey (Standard)")
ax1.set_ylabel('y(t)')
# plt.show()



ax2.plot(t,y_log[:, 0])
ax2.plot(t,y_log[:, 1])
ax2.set_xlabel('time')
ax2.set_title("Predator-Prey (Log)")
ax2.set_ylabel('ylog(t)')


ax3.plot(t,np.log(y[:, 0]))
ax3.plot(t,np.log(y[:, 1]))
ax3.set_xlabel('time')
ax3.set_title("Predator-Prey (Empirical Log)")
ax3.set_ylabel('y(t)')


ymax = max(y[:, 1])
# print(f"peak predator {i_max}\n Done!")

plt.show()
