def model_log(y, t):
    A = np.array([[0.7, 0.5], [0.8, 0.4]])
    exp_s = 0.9
    exp_i = 0.8

    predation = np.exp(exp_s*y[0] + exp_i*y[1])

    dydt = [A[0,0]*np.exp(y[0]) - A[0,1]*predation, A[1,1]*predation - A[1,0]*np.exp(y[1])]

    dydt_log = np.multiply(np.exp(-y), dydt)
    # dydt_log = dydt
    # dydt = -k * y
    return dydt_log

def model(y,t):
    # k = 0.3

    A = np.array([[0.7, 0.5], [0.8, 0.4]])
    exp_s = 0.9
    exp_i = 0.8

    dydt = [A[0,0]*y[0] - A[0,1]*(y[0]**exp_s)*(y[1]**exp_i), A[1,1]*(y[0]**exp_s)*(y[1]**exp_i) - A[1,0]*y[1]]

    # exp_s = 1
    # exp_i = 1

    # dydt = [-beta*(y[0]**exp_s)*(y[1]**exp_i), beta*(y[0]**exp_s)*(y[1]**exp_i) - gamma*y[1]]
    # dydt = -k * y
    return dydt

# https://matplotlib.org/stable/gallery/images_contours_and_fields/plot_streamplot.html
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint

w = 3
Y, X = np.mgrid[-w:w:100j, -w:w:100j]
A = np.array([[0.7, 0.5], [0.8, 0.4]])
exp_s = 0.9
exp_i = 0.8

# U = -1 - X**2 + Y
# V = 1 + X - Y**2
U = np.exp(-X) * ( A[0,0]*np.exp(X) - A[0, 1]*np.exp(exp_s*X+exp_i*Y))
V = np.exp(-Y) * (-A[1,0]*np.exp(Y) + A[1, 1]*np.exp(exp_s*X+exp_i*Y))



speed = np.sqrt(U**2 + V**2)

# fig, axs = plt.subplots(3, 2, figsize=(7, 9), height_ratios=[1, 1, 2])
# axs = axs.flat

# x0 = [0.5, 1]
x0 = [0.8, 1]
y0 = np.log(x0)

# fig, (ax1, ax2, ax3) = plt.subplots(1, 3)

#  Varying density along a streamline
plt.streamplot(X, Y, U, V)



#initial conditions
N = 100
# R = 0.5;
R = 0.4;
theta = np.linspace(0, 2*np.pi, N)
theta = theta[:, np.newaxis]
x0circ = R*np.concatenate((np.cos(theta), np.sin(theta)), axis=1) + np.transpose(x0)
y0circ = np.log(x0circ)

print("done")

Tmax = 20

# sample trajectories
Nt = 400
Tmax = 20
t = np.linspace(0, Tmax, Nt)

i_max = -np.Inf

y_center = odeint(model_log, y0, t)
i_max_center = max(y_center[:, 1])

for i in range(N):
    y_log = odeint(model_log, y0circ[i, :], t)
    # plt.plot(t, y_log[:, 0])
    # plt.plot(t, y_log[:, 1])
    plt.plot(y_log[:, 0], y_log[:, 1])
    i_max = max(i_max, max(y_log[:, 1]))


#plot initial conditions and center point
plt.plot(y0circ[:, 0], y0circ[:, 1], 'k')
plt.scatter(y0[0], y0[1], color='black')
plt.plot(y_center[:, 0], y_center[:, 1], 'k')
# disp("")
print(f"peak predator {i_max} (circle)\n peak predator {i_max_center} (center)\n Done!")
plt.xlabel("log(prey)")
plt.ylabel("log(predator)")
plt.show()