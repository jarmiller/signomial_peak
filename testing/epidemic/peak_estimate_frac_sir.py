#attempt peak estimation for a fractional SIR system
import numpy as np
import sageopt
import sageopt.coniclifts as cl

#helper functions
def gen_alpha(A1, d):
    # A0 = np.array(([[0, 0]]))
    A0 = np.zeros([1, A1.shape[1]])

    A_base = np.concatenate([A0, A1], 0)

    A = A_base
    for i in range(1, d):
        Akron = np.kron(np.ones((A_base.shape[0], 1)), A) + np.kron(A_base, np.ones((A.shape[0], 1)))
        A = np.unique(Akron, axis=0)

    return A

def sig_grad(sig, i):
    """ (trying to debug Signomial._partial from Signomials.py)
    Compute the symbolic partial derivative of this signomial, at coordinate ``i``.

    Parameters
    ----------
    i : int
        ``i`` must be an integer from 0 to ``self.n - 1``.

    Returns
    -------
    p : Signomial
        The function obtained by differentiating this signomial with respect to its i-th argument.
    """
    c_partial = cl.Expression(sig.c.copy())
    for j in range(sig._m):
        vec = sig.alpha[j, :]
        c_partial[j] = sig.c[j] * vec[i]

    p = sageopt.Signomial(sig.alpha, c_partial)
    return p

#define the objective
n = 2
vars = sageopt.standard_sig_monomials(n+1)
t = vars[0]
y = vars[1:]

#define the auxiliary function
A1_set = np.array([[1, 0, 0], [0, 0.5, 0], [0, -0.5, 0], [0, 0.8, 0], [0, -0.8, 0],  [0, 0, 0.5], [0, 0, -0.5]])
d = 1

#sampled (ODE) peak infected: 0.43404759817214705

#terms [1, 0, 0], [0, 0.5, 0], [0, -0.5, 0], [0, 0.8, 0], [0, -0.8, 0],  [0, 0, 0.5], [0, 0, -0.5]
#d=1:0.9993523500146635
#d=2:0.5056038361996538
#d=3:0.47113894251824157
#d=4: 0.4365666120

# A1_set = np.array([[0.5, 0, 0],  [1, 0, 0], [0, 0.5, 0], [0, -0.5, 0],  [0, 0.8, 0], [0, -0.8, 0], [0, 0, 0.5], [0, 0, -0.5]])
# d = 3

# 0.45248469850894346

v_alpha = gen_alpha(A1_set, d)
v_c = cl.Variable(shape=(v_alpha.shape[0],))

v = sageopt.Signomial(v_alpha, v_c)

#define the dynamics
beta = 1
gamma = 0.5
exp_s = 0.8
exp_i = 0.5

Tmax = 10;

#define the objective
p = y[1]

#dynamics are [f1, f2] in log-space
f1_c = np.array([-beta])
f1_alpha = np.array([[0, exp_s, exp_i]])
f1_alpha_log = f1_alpha - np.array([[0, 1, 0]])

f1 = sageopt.Signomial(f1_alpha_log, f1_c)

# f2_c = np.array([[beta], [-gamma]])
f2_c = np.array([beta, -gamma])
f2_alpha = np.array([[0, exp_s, exp_i], [0, 0, 1]])
f2_alpha_log = f2_alpha - np.array([[0, 0, 1]])
f2 = sageopt.Signomial(f2_alpha_log, f2_c)

#define the support sets
Tstart = 0
Tend = 1
# cons_t = [t-np.exp(Tstart), np.exp(Tstart+Tend)-t]
cons_t = [t-np.exp(Tstart), np.exp(Tend)-t]
cons_x = [y[0]-1e-6, y[1]-1e-6, 1-y[0]-y[1]]

cons_all = cons_t + cons_x



# sys_vars = cl.Variable(shape=(n+1,), name='temp')
# t_var = sys_vars[0]
# x_var = sys_vars[1:]

# cons_t = [t_var <=2, t_var>=Tstart]
# cons_x = []
# for i in range(n):
#     # cons_x.append(x_var[i] <= 1)
#     cons_x.append(x_var[i] >= np.log(1e-6))
#
# cons_x.append(sum(y) <= 1)
#
# cons_all = cons_t + cons_x

#time-space set
# Xall = sageopt.SigDomain(n+1)
# Xall.parse_coniclifts_constraints(cons_all)

#Initial condition

x0 = np.array([[np.exp(Tstart)], [0.9], [0.1]])
y0 = np.log(x0)

# ty0 = np.concatenate([[[1]], y0], axis=0)

v0 = v(y0)

objective = v0

#Lie nonnegativity

dvdt = sig_grad(v, 0)
dvdx = (sig_grad(v, 1)*f1) + (sig_grad(v, 2)*f2)

Lv = dvdt + Tmax*dvdx
nonneg_lie = -Lv
Xall_lie = sageopt.relaxations.sage_sigs.infer_domain(nonneg_lie, cons_all, [], True)
constr_lie = cl.PrimalSageCone(nonneg_lie.c, nonneg_lie.alpha, X=Xall_lie, name='lie')

#peak value
p = y[1]
nonneg_peak = v - p
Xall_peak = sageopt.relaxations.sage_sigs.infer_domain(nonneg_peak, cons_all, [], True)
constr_peak = cl.PrimalSageCone(nonneg_peak.c, nonneg_peak.alpha, X=Xall_peak, name='peak')


#put the problem together
# slack = cl.Variable(shape=(1,))
# constr_obj = slack <= objective
constr_obj = [objective <= 14, objective >= 0]
constr = [constr_lie, constr_peak]+ constr_obj


#solve the problem
prob = cl.Problem(cl.MIN, objective, constr)

status, val = prob.solve()
# solutions = sageopt.sig_solrec(prob)
print(f'peak bound {val}')
print("done!")


