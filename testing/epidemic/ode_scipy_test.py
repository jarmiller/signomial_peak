import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

# from the tutorial https://apmonitor.com/pdc/index.php/Main/SolveDifferentialEquations
# function that returns dy/dt
def model(y,t):
    # k = 0.3
    beta = 1
    gamma = 0.5
    exp_s = 0.8
    exp_i = 0.5


    # exp_s = 1
    # exp_i = 1

    dydt = [-beta*(y[0]**exp_s)*(y[1]**exp_i), beta*(y[0]**exp_s)*(y[1]**exp_i) - gamma*y[1]]
    # dydt = -k * y
    return dydt

def model_log(z, t):
    beta = 1
    gamma = 0.5
    exp_s = 0.8
    exp_i = 0.5


    # exp_s = 1
    # exp_i = 1

    cross_term = beta*np.exp(z[0]*exp_s + z[1]*exp_i)

    dydt_orig = [-cross_term, cross_term - gamma*np.exp(z[1])]
    dydt_log = np.multiply(np.exp(-z), dydt_orig)
    # dydt = -k * y
    return dydt_log

# initial condition
LOG = True
y0 = [0.9, 0.1]

y0_log = np.log(y0)


# time points
t = np.linspace(0,10, 400)

#sample equations
y = odeint(model,y0,t)
y_log = odeint(model_log,y0_log,t)

# solve ODE
# if LOG:
#     title = "Epidemic (Standard)"
# else:
#     title = "Epidemic (Log)"

# plot results

i_max = max(y[:, 1])

print(f'peak infected {i_max}')
fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
ax1.plot(t,y[:, 0])
ax1.plot(t,y[:, 1])
ax1.set_xlabel('time')
ax1.set_title("Epidemic (Standard)")
ax1.set_ylabel('y(t)')
# plt.show()



ax2.plot(t,y_log[:, 0])
ax2.plot(t,y_log[:, 1])
ax2.set_xlabel('time')
ax2.set_title("Epidemic (Log)")
ax2.set_ylabel('ylog(t)')


ax3.plot(t,np.log(y[:, 0]))
ax3.plot(t,np.log(y[:, 1]))
ax3.set_xlabel('time')
ax3.set_title("Epidemic (Empirical Log)")
ax3.set_ylabel('y(t)')

plt.show()
print("Done!")