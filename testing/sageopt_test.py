# -*- coding: utf-8 -*-
"""
Created on Wed Jan 10 15:44:53 2024

@author: jarmiller
"""

import sageopt.coniclifts as cl
import numpy as np
# random problem data
G = np.random.randn(3, 6)
h = np.random.rand(6)
c = np.random.rand(6)
# # input to coniclift's Problem constructor
# x = cl.Variable(shape=(6,))
# constrs = [0 <= x, G @ x == h]
# objective_expression = c @ x
# prob = cl.Problem(cl.MIN, objective_expression, constrs)
# prob.solve(solver='ECOS', verbose=False)
# x_opt = x.value