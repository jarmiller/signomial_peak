#attempt peak estimation for a fractional SIR system
import numpy as np
import sageopt
import sageopt.coniclifts as cl

#helper functions
def gen_alpha(A1, d):
    # A0 = np.array(([[0, 0]]))
    A0 = np.zeros([1, A1.shape[1]])

    A_base = np.concatenate([A0, A1], 0)

    A = A_base
    for i in range(1, d):
        Akron = np.kron(np.ones((A_base.shape[0], 1)), A) + np.kron(A_base, np.ones((A.shape[0], 1)))
        A = np.unique(Akron, axis=0)

    return A
def eval_time(sig, t):
    alpha_x = sig.alpha[:, 1:]
    c_x = cl.Expression(sig.c.copy())
    for j in range(sig._m):
        c_x[j] *= np.exp(t*sig.alpha[j, 0])

    p = sageopt.Signomial(alpha_x, c_x)
    return p


def sig_grad(sig, i):
    """ (trying to debug Signomial._partial from Signomials.py)
    Compute the symbolic partial derivative of this signomial, at coordinate ``i``.

    Parameters
    ----------
    i : int
        ``i`` must be an integer from 0 to ``self.n - 1``.

    Returns
    -------
    p : Signomial
        The function obtained by differentiating this signomial with respect to its i-th argument.
    """
    c_partial = cl.Expression(sig.c.copy())
    for j in range(sig._m):
        vec = sig.alpha[j, :]
        c_partial[j] = sig.c[j] * vec[i]

    p = sageopt.Signomial(sig.alpha, c_partial)
    return p

#define the objective
n = 2
vars = sageopt.standard_sig_monomials(n+1)
t = vars[0]
y = vars[1:]

#define the auxiliary function
# A1_set = np.array([[1, 0, 0], [0, 0.5, 0], [0, -0.5, 0], [0, 0.8, 0], [0, -0.8, 0],  [0, 0, 0.5], [0, 0, -0.5]])
# d = 3

A1_set = np.array([[1, 0, 0], [0, 1, 0], [0, -1, 0],  [0, 0, 1], [0, 0, -1]])

d=4
# 1.479534375e+00 UNKNOWN Time: 19.30

# d=3
#  1.928701191 time 3.92

# d=2
#   2.218147823e+00    Time: 0.23

# d=1
#   4.481689082  Time: 0.01

# A1_set = np.array([[1, 0, 0], [0, 1, 0], [0, -1, 0],  [0, 0, 1], [0, 0, -1],  [0, 0.4, 0], [0, 0, -0.4]])
# d = 1
#2.7182818556e+00 time 0.01

# d=2
#1.558866883 time 0.92

# d=3
#1.518140542, time 18.06

# d=4
#1.1631742367 UNKNOWN, time 105.53

#sample 1.4237725905155365

v_alpha = gen_alpha(A1_set, d)
v_c = cl.Variable(shape=(v_alpha.shape[0],))

v = sageopt.Signomial(v_alpha, v_c)

#define the dynamics
Tmax =5

#define the objective
p = y[0]

#dynamics are [f1, f2] in log-space
#mode 1
f1_c = np.array([1.2, -0.5, -0.8])
f1_alpha = np.array([[0, -1, 1], [0, -0.6, 0], [0, -1, -0.4]])

f1 = sageopt.Signomial(f1_alpha, f1_c)

# f2_c = np.array([[beta], [-gamma]])
f2_c = np.array([-1,0.25])
f2_alpha = np.array([[0, 0, 0], [0, -3, -1]])
f2 = sageopt.Signomial(f2_alpha, f2_c)

#mode 2

# g1_c = np.array([1.4, -0.3, -0.2])
g1_c = np.array([1.4, -0.3, -0.6])
g1_alpha = np.array([[0, -1, 1], [0, -0.6, 0], [0, -1, -0.4]])

g1 = sageopt.Signomial(f1_alpha, f1_c)

# f2_c = np.array([[beta], [-gamma]])
g2_c = np.array([-1.25, 0.15])
g2_alpha = np.array([[0, 0, 0], [0, -3, -1]])
g2 = sageopt.Signomial(f2_alpha, f2_c)

#define the support sets
sys_vars = cl.Variable(shape=(n+1,), name='temp')
t_var = sys_vars[0]
x_var = sys_vars[1:]

cons_t = [t_var <=1, t_var>=0]
cons_x = []
for i in range(n):
    cons_x.append(x_var[i] <= 1.5)
    cons_x.append(x_var[i] >= -1.5)



cons_all = cons_t + cons_x;
Xall = sageopt.SigDomain(n+1)
Xall.parse_coniclifts_constraints(cons_all)

x0_var = cl.Variable(shape=(n,), name='temp_0')
x0_expr = cl.Expression(x0_var.copy())
C0 = np.array([[-1], [-1]])
R = 0.3
# R = 1
cons_x0 = [cl.vector2norm(x0_expr-C0) <= R]

X0 = sageopt.SigDomain(n)
X0.parse_coniclifts_constraints(cons_x0)

# for i in range(n):
#     cons_x0.append(x0_var[i] <= 0.2)
#     cons_x0.append(x0_var[i] >= 0)

cons_all = cons_t + cons_x

#Lie nonnegativity
dvdt = sig_grad(v, 0)
dvdx_f = (sig_grad(v, 1)*f1) + (sig_grad(v, 2)*f2)
dvdx_g = (sig_grad(v, 1)*g1) + (sig_grad(v, 2)*g2)

Lv_f = dvdt + Tmax*dvdx_f
nonneg_lie_f = -Lv_f
constr_lie_f = cl.PrimalSageCone(nonneg_lie_f.c, nonneg_lie_f.alpha, X=Xall, name='lief')

Lv_g = dvdt + Tmax*dvdx_g
nonneg_lie_g = -Lv_g
# nonneg_lie_g = -Lv_f
constr_lie_g = cl.PrimalSageCone(nonneg_lie_g.c, nonneg_lie_g.alpha, X=Xall, name='lieg')

#peak value
nonneg_peak = v - p
constr_peak = cl.PrimalSageCone(nonneg_peak.c, nonneg_peak.alpha, X=Xall, name='peak')

#Initial condition
Tstart = 0


v0 = eval_time(v, Tstart)

gamma = cl.Variable(shape=(1,))
# nonneg_init = cl.Expression(gamma) - v0
nonneg_init = -v0
const_ind = nonneg_init.constant_location();
nonneg_init.c[const_ind] += gamma
constr_init = cl.PrimalSageCone(nonneg_init.c, nonneg_init.alpha, X=X0, name='peak')
objective = gamma

#put the problem together
# slack = cl.Variable(shape=(1,))
# constr_obj = slack <= objective
# constr_obj = [objective <= 14, objective >= 0]
constr = [constr_lie_g, constr_lie_f, constr_peak, constr_init]


#solve the problem
prob = cl.Problem(cl.MIN, objective, constr)

status, val = prob.solve()
# solutions = sageopt.sig_solrec(prob)
print(f'peak bound {val}')
print("done!")


