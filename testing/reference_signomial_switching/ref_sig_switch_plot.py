import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

# from the tutorial https://apmonitor.com/pdc/index.php/Main/SolveDifferentialEquations
# function that returns dy/dt
def model(y,t):
    X = y[0]
    Y = y[1]

    # U_exp = np.exp(Y) - 1.2 * np.exp(0.4 * X) - np.exp(-0.4 * Y)
    # V_exp = -np.exp(Y) - np.exp(X) + 0.25 * (np.exp(-3 * X)) + np.exp(-X)
    # U = U_exp
    # V = V_exp


    U_exp = 1.2 * np.exp(Y) - 0.5*np.exp(0.4 * X) - 0.8 * np.exp(-0.4 * Y)
    V_exp = -np.exp(Y) + 0.25 * (np.exp(-3 * X)) #+ 0.4*(np.exp(-0.1*X)- np.exp(0.1*X))
    U = np.exp(-X) * (U_exp)
    V = np.exp(-Y) * (V_exp)

    dydt = [U, V]
    return dydt

def model2(y,t):
    X = y[0]
    Y = y[1]

    # U_exp = np.exp(Y) - 1.2 * np.exp(0.4 * X) - np.exp(-0.4 * Y)
    # V_exp = -np.exp(Y) - np.exp(X) + 0.25 * (np.exp(-3 * X)) + np.exp(-X)
    # U = U_exp
    # V = V_exp


    # U_exp = 1.4 * np.exp(Y) - 0.3*np.exp(0.4 * X) - 0.2 * np.exp(-0.4 * Y)
    # V_exp = -1.25*np.exp(Y) + 0.15 * (np.exp(-3 * X))# + 0.4*(np.exp(-0.1*X)- np.exp(0.1*X))

    U_exp = 1.4 * np.exp(Y) - 0.3 * np.exp(0.4 * X) - 0.6 * np.exp(-0.4 * Y)
    V_exp = -1.25 * np.exp(Y) + 0.15 * (np.exp(-3 * X))  # + 0.4*(np.exp(-0.1*X)- np.exp(0.1*X))
    U = np.exp(-X) * (U_exp)
    V = np.exp(-Y) * (V_exp)

    dydt = [U, V]
    return dydt



# initial condition

# time points
Tmax = 5
N = 400
t = np.linspace(0,Tmax, N)
# Npts = 500
Npts = 50000
th = np.linspace(0, np.pi*2, Npts)
C0 = [-1, -1]
mu = 0.2

rng = np.random.default_rng(30)
# rng.seed(0)

# print(switch_time)
# C0 = [-1.5, -1]

# C0 = [-3, -3]

i_max = -np.Inf
R = 0.3

fig, (ax1) = plt.subplots(1, 1)

for i in range(Npts):
#     # y0 = [R*np.cos(th[i]) + C0[0], R*np.sin(th[i]) + C0[1]]
#     y0 = C0
    thcurr = np.random.rand()*2*np.pi;
    y0 = [R*np.cos(thcurr) + C0[0], R*np.sin(thcurr) + C0[1]]
    sys = rng.integers(low=0, high=1)
    t0 = 0
    y = np.zeros((0, 2))
    # t = np.zeros((0))
    while t0 < Tmax:
        switch_time = rng.exponential(scale=mu)
        tend = min(t0 + switch_time, Tmax)

        steps = int(np.ceil(N * (tend - t0) * Tmax))
        tsample = np.linspace(t0, tend, steps)
        if sys==0:
            ycurr = odeint(model, y0, tsample - t0)
            # ycurr = odeint(model2, y0, tsample - t0)
        else:
            ycurr = odeint(model2, y0, tsample - t0)
            # ycurr = odeint(model, y0, tsample - t0)

        i_max_curr = max(ycurr[:, 0])
        i_max = max([i_max_curr, i_max])
        y = np.concatenate((y, ycurr), axis=0)
        # t = np.concatenate((t, tsample), axis=0)


        #go to next system
        sys = 1-sys
        t0 = tend
        y0 = y[-1, :]
    # ax1.plot(y[:, 0], y[:, 1])



#     y = odeint(model, y0, t)
#     i_max_curr = max(y[:, 1])
#     i_max = max([i_max_curr, i_max])
# y = odeint(model, y0, t)
#
# y2 = odeint(model2, y0, t)
# ax1.plot(y2[:, 1], y2[:, 0])
#
#
ax1.plot(R*np.cos(th)+C0[0], R*np.sin(th)+C0[1], 'k')
#

#sample equations



ei_max = np.exp(i_max)
#
# fig, (ax1) = plt.subplots(1, 1)
# ax1.plot(t,y[:, 0])
# ax1.plot(t,y[:, 1])
# ax1.set_xlabel('time')
# ax1.set_title("Reference (Standard)")
# ax1.set_ylabel('y(t)')
# plt.show()

pbar =  1.518140542


print(f"peak value {i_max}, exp {ei_max}")
yl = plt.gca().get_ylim()
plt.plot(np.log(pbar)*np.array([1,1]), yl, 'k--')
plt.ylim(yl)
plt.gca().set_aspect('equal')
plt.xlabel(r'$y_1$')
plt.ylabel(r'$y_2$')

# plt.savefig("ref_sig_plot_switch_k3_large.png", dpi=250)


plt.show()

print("Done!")

