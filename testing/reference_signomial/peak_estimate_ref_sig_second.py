#attempt peak estimation for a fractional SIR system
import numpy as np
import sageopt
import sageopt.coniclifts as cl

#helper functions
def gen_alpha(A1, d):
    # A0 = np.array(([[0, 0]]))
    A0 = np.zeros([1, A1.shape[1]])

    A_base = np.concatenate([A0, A1], 0)

    A = A_base
    for i in range(1, d):
        Akron = np.kron(np.ones((A_base.shape[0], 1)), A) + np.kron(A_base, np.ones((A.shape[0], 1)))
        A = np.unique(Akron, axis=0)

    return A
def eval_time(sig, t):
    alpha_x = sig.alpha[:, 1:]
    c_x = cl.Expression(sig.c.copy())
    for j in range(sig._m):
        c_x[j] *= np.exp(t*sig.alpha[j, 0])

    p = sageopt.Signomial(alpha_x, c_x)
    return p


def sig_grad(sig, i):
    """ (trying to debug Signomial._partial from Signomials.py)
    Compute the symbolic partial derivative of this signomial, at coordinate ``i``.

    Parameters
    ----------
    i : int
        ``i`` must be an integer from 0 to ``self.n - 1``.

    Returns
    -------
    p : Signomial
        The function obtained by differentiating this signomial with respect to its i-th argument.
    """
    c_partial = cl.Expression(sig.c.copy())
    for j in range(sig._m):
        vec = sig.alpha[j, :]
        c_partial[j] = sig.c[j] * vec[i]

    p = sageopt.Signomial(sig.alpha, c_partial)
    return p

#define the objective
n = 2
vars = sageopt.standard_sig_monomials(n+1)
t = vars[0]
y = vars[1:]

#define the auxiliary function
# A1_set = np.array([[1, 0, 0], [0, 0.5, 0], [0, -0.5, 0], [0, 0.8, 0], [0, -0.8, 0],  [0, 0, 0.5], [0, 0, -0.5]])
# d = 3

# A1_set = np.array([[1, 0, 0], [0, 0.5, 0], [0, -0.5, 0],  [0, 0, 0.5], [0, 0, -0.5]])

#peak value 0.15043296964222566, exp 1.1623373906006997

#stable until d=4
# A1_set = np.array([[1, 0, 0], [0, 1, 0], [0, -1, 0],  [0, 0, 1], [0, 0, -1]])
# d=1#: 2.7182818289e+00, time 0.01
# d=2#:  2.2181476909e+00, time = 0.14
# d=3#: 1.914670204e, time = 1.84
# d=4#: 1.515792433e+00, time=9.55



A1_set = np.array([[1, 0, 0], [0, 1, 0], [0, -1, 0],  [0, 0, 1], [0, 0, -1],  [0, 0.4, 0], [0, 0, -0.4]])
# d=1#: 2.7182818289e+00, time 0.02
# d=2#:  1.558866691e+00, time = 0.42
# d=3#: 1.520752984, time = 10.42
# d=4#:  1.169270065 UNKNOWN, time=60.17


INIT_POINT = False


v_alpha = gen_alpha(A1_set, d)
v_c = cl.Variable(shape=(v_alpha.shape[0],))

v = sageopt.Signomial(v_alpha, v_c)

#define the dynamics
Tmax =5

#define the objective
p = y[0]

#dynamics are [f1, f2] in log-space
f1_c = np.array([1.2, -0.5, -0.8])
f1_alpha = np.array([[0, 0, 1], [0, 0.4, 0], [0, 0, -0.4]])
f1_alpha_log = f1_alpha - np.array([[0, 1, 0]])
f1 = sageopt.Signomial(f1_alpha_log, f1_c)

# f2_c = np.array([[beta], [-gamma]])
f2_c = np.array([-1, 0.25])
f2_alpha = np.array([[0,0,1], [0, -3, 0]])
f2_alpha_log = f2_alpha - np.array([[0, 0, 1]])
f2 = sageopt.Signomial(f2_alpha_log, f2_c)

#define the support sets
sys_vars = cl.Variable(shape=(n+1,), name='temp')
t_var = sys_vars[0]
x_var = sys_vars[1:]

cons_t = [t_var <=1, t_var>=0]
cons_x = []
for i in range(n):
    cons_x.append(x_var[i] <= 1)
    cons_x.append(x_var[i] >= -1.5)

cons_all = cons_t + cons_x;
Xall = sageopt.SigDomain(n+1)
Xall.parse_coniclifts_constraints(cons_all)

x0_var = cl.Variable(shape=(n,), name='temp_0')
x0_expr = cl.Expression(x0_var.copy()).T
C0 = np.array([[-1], [-1]])
R = 0.3
cons_x0 = [cl.vector2norm(x0_expr-C0) <= R]

X0 = sageopt.SigDomain(n)
X0.parse_coniclifts_constraints(cons_x0)

# for i in range(n):
#     cons_x0.append(x0_var[i] <= 0.2)
#     cons_x0.append(x0_var[i] >= 0)

cons_all = cons_t + cons_x

#Lie nonnegativity
dvdt = sig_grad(v, 0)
dvdx = (sig_grad(v, 1)*f1) + (sig_grad(v, 2)*f2)

Lv = dvdt + Tmax*dvdx
nonneg_lie = -Lv
constr_lie = cl.PrimalSageCone(nonneg_lie.c, nonneg_lie.alpha, X=Xall, name='lie')

#peak value
nonneg_peak = v - p
constr_peak = cl.PrimalSageCone(nonneg_peak.c, nonneg_peak.alpha, X=Xall, name='peak')

#Initial condition
Tstart = 0


v0 = eval_time(v, Tstart)

constr = [constr_lie, constr_peak]

if INIT_POINT:
    v00 = v0(C0)
    objective = v00
    gamma = v00
    nonneg_init = v00

else:
    gamma = cl.Variable(shape=(1,))
    # nonneg_init = cl.Expression(gamma) - v0
    nonneg_init = -v0
    const_ind = nonneg_init.constant_location();
    nonneg_init.c[const_ind] += gamma
    constr_init = cl.PrimalSageCone(nonneg_init.c, nonneg_init.alpha, X=X0, name='peak')
    objective = gamma
    constr = constr + [constr_init]

#put the problem together
# slack = cl.Variable(shape=(1,))
# constr_obj = slack <= objective



#solve the problem
prob = cl.Problem(cl.MIN, objective, constr)

status, val = prob.solve()
# solutions = sageopt.sig_solrec(prob)


#recover the solution
vc_rec = v.c.value;
v_rec = sageopt.Signomial(v_alpha, vc_rec)
gam_rec = gamma.value

if not INIT_POINT:
    nonneg_init_rec = sageopt.Signomial(nonneg_init.alpha, nonneg_init.c.value)
else:
    nonneg_init_rec = v00.value

nonneg_peak_rec = sageopt.Signomial(nonneg_peak.alpha, nonneg_peak.c.value)
nonneg_lie_rec = sageopt.Signomial(nonneg_lie.alpha, nonneg_lie.c.value)



print(f'peak bound {val}')
print("done!")


