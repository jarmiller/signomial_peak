import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

# from the tutorial https://apmonitor.com/pdc/index.php/Main/SolveDifferentialEquations
# function that returns dy/dt
def model(y,t):
    X = y[0]
    Y = y[1]

    # U_exp = np.exp(Y) - 1.2 * np.exp(0.4 * X) - np.exp(-0.4 * Y)
    # V_exp = -np.exp(Y) - np.exp(X) + 0.25 * (np.exp(-3 * X)) + np.exp(-X)
    # U = U_exp
    # V = V_exp


    U_exp = 1.2 * np.exp(Y) - 0.5*np.exp(0.4 * X) - 0.8 * np.exp(-0.4 * Y)
    V_exp = -np.exp(Y) + 0.25 * (np.exp(-3 * X)) #+ 0.4*(np.exp(-0.1*X)- np.exp(0.1*X))
    U = np.exp(-X) * (U_exp)
    V = np.exp(-Y) * (V_exp)

    dydt = [U, V]
    return dydt



# initial condition

# time points
Tmax = 5
t = np.linspace(0,Tmax, 400)
Npts = 200
th = np.linspace(0, np.pi*2, Npts)
C0 = [-1, -1]

# C0 = [-1.5, -1]

# C0 = [-3, -3]

i_max = -np.Inf
R = 0.3

fig, (ax1) = plt.subplots(1, 1)
for i in range(Npts):
    y0 = [R*np.cos(th[i]) + C0[0], R*np.sin(th[i]) + C0[1]]
    y = odeint(model, y0, t)
    i_max_curr = max(y[:, 0])
    i_max = max([i_max_curr, i_max])
    ax1.plot(y[:, 0], y[:, 1])


ax1.plot(R*np.cos(th)+C0[0], R*np.sin(th)+C0[1], 'k')


#sample equations



ei_max = np.exp(i_max)
#
# fig, (ax1) = plt.subplots(1, 1)
# ax1.plot(t,y[:, 0])
# ax1.plot(t,y[:, 1])
# ax1.set_xlabel('time')
# ax1.set_title("Reference (Standard)")
# ax1.set_ylabel('y(t)')
# plt.show()

pbar = 1.515792433


print(f"peak value {i_max}, exp {ei_max}")
yl = plt.gca().get_ylim()
plt.plot(np.log(pbar)*np.array([1,1]), yl, 'k--')
plt.ylim(yl)
plt.gca().set_aspect('equal')
plt.xlabel(r'$y_1$')
plt.ylabel(r'$y_2$')

plt.savefig("ref_sig_plot_k4.png", dpi=250)


plt.show()

print("Done!")