# https://matplotlib.org/stable/gallery/images_contours_and_fields/plot_streamplot.html
import matplotlib.pyplot as plt
import numpy as np

w = 3
Y, X = np.mgrid[-w:w:100j, -w:w:100j]
# U = -1 - X**2 + Y
# V = 1 + X - Y**2
# U = -np.exp(0.5*X) - 2*np.exp(-Y) +np.exp(-2.5*X + 3.5*Y)+ 0.3*np.exp(Y)
# V = -np.exp(0.5*Y) + np.exp(2.2*X -3.1*Y) - np.exp(-Y+X) + 0.3*np.exp(X)


U_exp = 1.2*np.exp(Y) -0.5*np.exp(0.4*X) - 0.8*np.exp(-0.4*Y)
V_exp = -np.exp(Y) + 0.25*(np.exp(-3*X)) #+ 0.2*(np.exp(-0.1*X)- np.exp(0.1*X))
# V_exp = -np.exp(X) + np.exp(0.3*X + 0.6*Y) + 0.2*np.exp(0.8*X - 0.3*Y)
# V_exp = -np.exp(Y) - np.exp(X) + (1/3)*(np.exp(-3*X)) + np.exp(-X)

U = np.exp(-X) * (U_exp)
V = np.exp(-Y) * (V_exp)


speed = np.sqrt(U**2 + V**2)

# fig, axs = plt.subplots(3, 2, figsize=(7, 9), height_ratios=[1, 1, 2])
# axs = axs.flatfsdf

#  Varying density along a streamline
plt.streamplot(X, Y, U, V)
plt.show()

