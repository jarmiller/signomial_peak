
#from https://www.quora.com/How-can-I-generate-a-3D-streamplot-to-visualize-fluid-flow-in-Python
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


# Define the vector field function
def vector_field(x, y, z):
    return (-y, x, z)


# Define the domain of the vector field
x = np.linspace(-5, 5, 20)
y = np.linspace(-5, 5, 20)
z = np.linspace(-5, 5, 20)

X, Y, Z = np.meshgrid(x, y, z)

# Compute the vector field values at each point
U, V, W = vector_field(X, Y, Z)

# Create a 3D figure
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# Generate the streamplot
# ax.streamplot3D(X, Y, Z, U, V, W, linewidth=0.8, cmap='viridis')

# Set labels and title
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
ax.set_title('3D Streamplot of Vector Field')

# Show the plot
plt.show()