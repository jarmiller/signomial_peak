import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# from the tutorial https://apmonitor.com/pdc/index.php/Main/SolveDifferentialEquations
# function that returns dy/dt
def model(y,t):
    X = y[0]
    Y = y[1]
    Z = y[2]

    # U_exp = np.exp(Y) - 1.2 * np.exp(0.4 * X) - np.exp(-0.4 * Y)
    # V_exp = -np.exp(Y) - np.exp(X) + 0.25 * (np.exp(-3 * X)) + np.exp(-X)
    # U = U_exp
    # V = V_exp


    # U = -X
    # V = -Y
    # W = -Z

    # U_exp = np.exp(Y) -np.exp(-Y) - 0.5 * np.exp(-0.3*X - 0.5*Z)
    # V_exp = -np.exp(Y) + 0.25 * (np.exp(-3 * X)) #+ 0.4*(np.exp(-0.1*X)- np.exp(0.1*X))
    # W_exp = -0.5*np.exp(0.5 * X) - np.exp(Z)
    U_exp =  np.exp(0.6*Y + Z) - np.exp(0.2 * X - 0.6*Y)
    V_exp = -np.exp(0.6*Y + 0.2*Z) + 5*(np.exp(-2 * X + Z))  # + 0.2*(np.exp(-0.1*X)- np.exp(0.1*X))
    W_exp = np.exp(-Z) - np.exp(Z) - 2*np.exp(0.6 *X + 0.6*Y)
    U = np.exp(-X) * (U_exp)
    V = np.exp(-Y) * (V_exp)
    W = np.exp(-Z) * (W_exp)

    dydt = [U, V, W]
    return dydt



# initial condition

# time points
Tmax = 5
t = np.linspace(0,Tmax, 400)
# Npts = 30
Npts = 30
th = np.linspace(0, np.pi*2, Npts)
ph = np.linspace(0, np.pi, Npts)
C0 = [-1, -1, -1]

# C0 = [-1.5, -1]

# C0 = [-3, -3]

i_max = -np.Inf
R = 0.2



# fig, (ax1) = plt.subplots(1, 1)
# Create a 3D figure
ax = plt.figure().add_subplot(projection='3d')

for i in range(Npts):
    for j in range(Npts):
        y0 = [R*np.cos(th[i])*np.sin(ph[j]) + C0[0], R*np.sin(th[i])*np.sin(ph[j]) + C0[1], R*np.cos(ph[j]) + C0[2]]
        y = odeint(model, y0, t)
        i_max_curr = max(y[:, 0])
        i_max = max([i_max_curr, i_max])
        ax.plot(y[:, 0], y[:, 1], y[:, 2])



# ax1.plot(R*np.cos(th)+C0[0], R*np.sin(th)+C0[1], 'k')


#sample equations

ei_max = np.exp(i_max)
print(i_max, ', ' , ei_max)
#
# fig, (ax1) = plt.subplots(1, 1)
# ax1.plot(t,y[:, 0])
# ax1.plot(t,y[:, 1])
# ax1.set_xlabel('time')
# ax1.set_title("Reference (Standard)")
# ax1.set_ylabel('y(t)')
# plt.show()

pbar = np.log(1.234844768)


# print(f"peak value {i_max}, exp {ei_max}")
xl = plt.gca().get_xlim()
yl = plt.gca().get_ylim()
zl = plt.gca().get_zlim()
# plt.plot(yl, np.log(pbar)*np.array([1,1]), 'k--')
# plt.xlim([-5, 5])
# plt.ylim([-5, 5])
# plt.zlim([-5, 5])
# plt.gca().set_aspect('equal')

ax.set_xlabel(r'$y_1$')
ax.set_ylabel(r'$y_2$')
ax.set_zlabel(r'$y_3$')

yyl, zzl = np.meshgrid(yl, zl)
ax.plot_surface(pbar*np.ones_like(yyl), yyl, zzl)
# pr = Rectangle([xl[0], yl[0]], xl[1], yl[1])
# ax.add_patch(pr)
# art3d.pathpatch_2d_to_3d(p, z=0, zdir="x")

# ax.plot_surface(xl, yl, pbar, alpha=0.2)

# plt.savefig("ref_sig_plot_k4.png", dpi=250)
#

plt.show()

print("Done!")
plt.savefig("img/test_3d_d4.png", dpi=250)
