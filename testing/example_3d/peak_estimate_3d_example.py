#attempt peak estimation for a 3d dynamical system
import numpy as np
import sageopt
import sageopt.coniclifts as cl

#helper functions
def gen_alpha(A1, d):
    # A0 = np.array(([[0, 0]]))
    A0 = np.zeros([1, A1.shape[1]])

    A_base = np.concatenate([A0, A1], 0)

    A = A_base
    for i in range(1, d):
        Akron = np.kron(np.ones((A_base.shape[0], 1)), A) + np.kron(A_base, np.ones((A.shape[0], 1)))
        A = np.unique(Akron, axis=0)

    return A
def eval_time(sig, t):
    alpha_x = sig.alpha[:, 1:]
    c_x = cl.Expression(sig.c.copy())
    for j in range(sig._m):
        c_x[j] *= np.exp(t*sig.alpha[j, 0])

    p = sageopt.Signomial(alpha_x, c_x)
    return p


def sig_grad(sig, i):
    """ (trying to debug Signomial._partial from Signomials.py)
    Compute the symbolic partial derivative of this signomial, at coordinate ``i``.

    Parameters
    ----------
    i : int
        ``i`` must be an integer from 0 to ``self.n - 1``.

    Returns
    -------
    p : Signomial
        The function obtained by differentiating this signomial with respect to its i-th argument.
    """
    c_partial = cl.Expression(sig.c.copy())
    for j in range(sig._m):
        vec = sig.alpha[j, :]
        c_partial[j] = sig.c[j] * vec[i]

    p = sageopt.Signomial(sig.alpha, c_partial)
    return p

#define the objective
n = 3
vars = sageopt.standard_sig_monomials(n+1)
t = vars[0]
y = vars[1:]

#define the auxiliary function
# A1_set = np.array([[1, 0, 0], [0, 0.5, 0], [0, -0.5, 0], [0, 0.8, 0], [0, -0.8, 0],  [0, 0, 0.5], [0, 0, -0.5]])
# d = 3

# A1_set = np.array([[1, 0, 0], [0, 0.5, 0], [0, -0.5, 0],  [0, 0, 0.5], [0, 0, -0.5]])
A1_set = np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1],
                   [0, 0.2, 0, 0], [0, 0, 0, 0.2], [0, 0.6,0,  0], [0, 0, 0.6,  0]])
d=2


#array [1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1],
# [0, 0.2, 0, 0], [0, 0, 0, 0.2], [0, 0.6,0,  0], [0, 0, 0.6,  0]
#Tmax = 8
#p = y[1] (e^x[1])
#d=1: 2.793540519, time 0.01
#d=2: 1.7123995052e, time 1.86
#d=3: 1.5309643372e+00
#d=4: 4.3689236629e-01 #INVALID BOUND UNKNOWN
#d=5: out of memory

#Tmax = 6
#d=1: 2.7935405190e+00, time = 0.01
#d=2:  1.7123995066e+00, time = 1.84
#d=3: 1.234844768, time=74.11
#d=4: UNKNOWN (

v_alpha = gen_alpha(A1_set, d)
v_c = cl.Variable(shape=(v_alpha.shape[0],))

v = sageopt.Signomial(v_alpha, v_c)

#define the dynamics
Tmax =6

#define the objective
p = y[0]

#dynamics are [f1, f2] in log-space
f1_c = np.array([1, -1])
f1_alpha = np.array([[0, 0,  0.6, 1], [0, 0.2, -0.6, 0]])

f1 = sageopt.Signomial(f1_alpha, f1_c)

# f2_c = np.array([[beta], [-gamma]])
f2_c = np.array([-1, 5])
f2_alpha = np.array([[0, 0, 0.6, 0.2], [0, -2, 0, 1]])
f2 = sageopt.Signomial(f2_alpha, f2_c)

# f2_c = np.array([[beta], [-gamma]])
f3_c = np.array([1, -1, -2])
f3_alpha = np.array([[0, 0, 0, -1], [0, 0, 0, 1], [0, 0.6, 0.6, 0]])
f3 = sageopt.Signomial(f3_alpha, f3_c)

#define the support sets
sys_vars = cl.Variable(shape=(n+1,), name='temp')
t_var = sys_vars[0]
x_var = sys_vars[1:]

cons_t = [t_var <=1, t_var>=0]
cons_x = []
for i in range(n):
    cons_x.append(x_var[i] <= 1.5)
    cons_x.append(x_var[i] >= -1.5)

cons_all = cons_t + cons_x;
Xall = sageopt.SigDomain(n+1)
Xall.parse_coniclifts_constraints(cons_all)

x0_var = cl.Variable(shape=(n,), name='temp_0')
x0_expr = cl.Expression(x0_var.copy())
C0 = np.array([[-1], [-1], [-1]])
R0 = 0.2
cons_x0 = [cl.vector2norm(x0_expr-C0) <= R0]

X0 = sageopt.SigDomain(n)
X0.parse_coniclifts_constraints(cons_x0)

# for i in range(n):
#     cons_x0.append(x0_var[i] <= 0.2)
#     cons_x0.append(x0_var[i] >= 0)

cons_all = cons_t + cons_x

#Lie nonnegativity
dvdt = sig_grad(v, 0)
dvdx = (sig_grad(v, 1)*f1) + (sig_grad(v, 2)*f2) + (sig_grad(v, 3)*f3)

Lv = dvdt + Tmax*dvdx
nonneg_lie = -Lv
constr_lie = cl.PrimalSageCone(nonneg_lie.c, nonneg_lie.alpha, X=Xall, name='lie')

#peak value
nonneg_peak = v - p
constr_peak = cl.PrimalSageCone(nonneg_peak.c, nonneg_peak.alpha, X=Xall, name='peak')

#Initial condition
Tstart = 0


v0 = eval_time(v, Tstart)

gamma = cl.Variable(shape=(1,))
# nonneg_init = cl.Expression(gamma) - v0
nonneg_init = -v0
const_ind = nonneg_init.constant_location();
nonneg_init.c[const_ind] += gamma
constr_init = cl.PrimalSageCone(nonneg_init.c, nonneg_init.alpha, X=X0, name='peak')
objective = gamma

#put the problem together
# slack = cl.Variable(shape=(1,))
# constr_obj = slack <= objective
constr_obj = [objective <= 14, objective >= 0]
constr = [constr_lie, constr_peak, constr_init]+ constr_obj


#solve the problem
prob = cl.Problem(cl.MIN, objective, constr)

status, val = prob.solve()
# solutions = sageopt.sig_solrec(prob)
print(f'peak bound {val}')
print("done!")


